import axios from 'axios'
import { BASE_URL, TIMEOUT } from './config'
import useMainStore from '@/stores/modules/main'

const mainStore = useMainStore()

class HYRequest {
  // 在创建 axios 实例时，传入公共配置
  constructor(baseURL, timeout=10000) {
    this.instance = axios.create({
      baseURL,
      timeout
    })

    // 请求拦截器
    this.instance.interceptors.request.use(config => {
      // 网络请求前进行的操作
      mainStore.isLoading = true
      return config
    }, err => {
      // 网络请求失败进行的操作
      return err
    })

    // 响应拦截器
    this.instance.interceptors.response.use(res => {
      // 服务器响应成功进行的操作
      mainStore.isLoading = false
      return res
    }, err => {
      // 服务器响应失败进行的操作
      mainStore.isLoading = false
      return err
    })
  }

  

  // 封装 request 方法
  request(config) {
    // 返回一个 Promise 对象
    return new Promise((resolve, reject) => {
      // 使用 axios.request() 发送请求
      this.instance.request(config).then(res => {
        // 返回请求成功的数据
        resolve(res.data)
      }).catch(err => {
        // 返回请求失败的相关信息
        reject(err)
      })
    })
  }

  // 封装 get 方法
  get(config) {
    return this.instance.request({ ...config, method: 'get' })
  }

  // 封装 post 方法
  post(config) {
    return this.instance.request({ ...config, method: 'post' })
  }
}

// 1.需要使用一个 axios 实例
export default new HYRequest(BASE_URL, TIMEOUT)
// 2.需要使用多个 axios 实例
// export default HYRequest



