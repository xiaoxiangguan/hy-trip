import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  // 设置路由模式
  history: new createWebHashHistory(),
  // 配置路由映射关系：path => component
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      name: 'home',
      path: '/home',
      // 路由懒加载，import() 引入的文件在打包时会进行分包处理
      component: () => import('@/views/home/home.vue')
    },
    {
      
      name: 'favor',
      path: '/favor',
      component: () => import('@/views/favor/favor.vue')
    },
    {
      
      name: 'message',
      path: '/message',
      component: () => import('@/views/message/message.vue'),
      meta: {
        hideTabBar: true
      }
    },
    {
      
      name: 'order',
      path: '/order',
      component: () => import('@/views/order/order.vue')
    },
    {
      
      name: 'city',
      path: '/city',
      component: () => import('@/views/city/city.vue'),
      meta: {
        hideTabBar: true
      }
    },
    {
      
      name: 'search',
      path: '/search',
      component: () => import('@/views/search/search.vue'),
      meta: {
        hideTabBar: true
      }
    },
    {
      // 接收动态参数 id
      // 在组件中通过 $route.params.id 使用该动态参数
      path: '/detail/:id',
      component: () => import('@/views/detail/detail.vue'),
    },
  ]
})

export default router