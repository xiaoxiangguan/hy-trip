// 安装 dayjs 第三方库：npm install dayjs
import dayjs from 'dayjs'

// 日期格式化函数
export function formatMonthDay(date, formatStr = 'MM月DD日') {
  return dayjs(date).format(formatStr)
}

// 计算时间差
export function getDiffDays(startDate, endDate) {
  // diff() 参数2为相差时间戳的转换单位
  return dayjs(endDate).diff(startDate, 'day') + 1
}