export const getAssetURL = (image) => {
  // 参数一：相对路径
  // 参数二：当前文件的路径
  // new URL() 的作用：将参数一拼接在参数二的后面
  // console.log(new URL(`../assets/img/${image}`, import.meta.url))
  return new URL(`../assets/img/${image}`, import.meta.url).href
}