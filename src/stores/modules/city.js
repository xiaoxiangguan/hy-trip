import { defineStore } from "pinia"
import { getCityAll } from "@/services"

const useCityStore = defineStore('city', {
  state: () => ({
    allCities: {},
    currentCity: {
      cityName: '广州'
    }
  }),
  actions: {
    async fetchAllCitiesData() {
      const res = await getCityAll()
      // 可通过 this 访问整个 store 实例
      this.allCities = res.data.data
    }
  }
})

export default useCityStore