import { defineStore } from 'pinia'
import { getHotSuggests, getCategories, getHouseList } from '@/services'

const useHomeStore = defineStore('home', {
  state: () => ({
    hotSuggests: {},
    categories: [],
    // 下一次请求的页数
    currentPage: 1,
    houseList: []
  }),
  actions: {
    async fetchHotSuggestsData() {
      const res = await getHotSuggests()
      this.hotSuggests = res.data.data
    },
    async fetchCategoriesData() {
      const res = await getCategories()
      this.categories = res.data.data
    },
    async fetchHouseListData() {
      const res = await getHouseList(this.currentPage)
      this.houseList.push(...res.data.data)
      // 每次请求完数据后，currentPage 自增 1，作为下一次请求的页数
      this.currentPage++
    }
  }
})

export default useHomeStore