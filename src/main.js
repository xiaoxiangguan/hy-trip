import { createApp } from 'vue'
import App from './App.vue'

// 引入 css 样式
import 'normalize.css'
import '@/assets/css/index.css'

// 引入路由映射关系
import router from './router'

// 引入状态管理 pinia
import pinia from './stores'

const app = createApp(App)


app.use(router)
app.use(pinia)

app.mount('#app')