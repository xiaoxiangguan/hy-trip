module.exports = {
  plugins: {
    'postcss-px-to-viewport': {
      viewportWidth: 375, // UI设计稿的视口宽度
      selectorBlackList: [".content"] // css选择器黑名单：里面的css选择器中的单位不会进行转换
    },
  },
};